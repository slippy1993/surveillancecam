import requests
import json
import time
import pytz
from datetime import time as dt
from datetime import datetime


class TelegramApi():
    def __init__(self,token="YOURTOKEN",id = YOURID):
        self.token = token
        self.chat_id = YOURID#id  # chat id
        self.file = ""
        self.Armed = True
        self.ArmedByTimeLower = dt(hour = 23, minute = 00, second = 00)
        self.ArmedByTimeUpper = dt(hour = 23, minute = 1, second = 00)
        self.ForceArmed = False
        self.DateForceArmed = 0
    def GetDateTime(self):
        # datetime object containing current date and time
        now = datetime.now()

        # dd/mm/YY H:M:S
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return dt_string
    def SendPhoto(self,fileName):
        if(self.Armed ==  True):
            url = f"https://api.telegram.org/bot"+self.token+"/sendPhoto"
            files = {}
            dt_String = self.GetDateTime()
            files["photo"] = open(fileName, "rb")
            try:
                requests.get(url, params={"chat_id": self.chat_id,"caption":dt_String}, files=files)
            except:
                pass

    def SendLive(self,fileName):
        if(True):
            url = f"https://api.telegram.org/bot"+self.token+"/sendPhoto"
            files = {}
            dt_String = self.GetDateTime()
            files["photo"] = open(fileName, "rb")
            print("senidng live! ")
            try:
                requests.get(url, params={"chat_id": self.chat_id,"caption":dt_String}, files=files)
            except:
                pass

    def SendMessage(self,message="Dummy"):
        if(self.Armed == True or self.Armed == False):
            params = {"chat_id": self.chat_id, "text":message}
            url = f"https://api.telegram.org/bot"+self.token+"/sendMessage"
            try:
                message = requests.post(url, params=params)
            except:
                pass
            
    def ReadLatestMessages(self):
        try:
            answer = requests.get(f"https://api.telegram.org/bot"+self.token+"/getUpdates")
            content = answer.content
            data = json.loads(content)
            
            return data['result'][-1]['message']['text'], data['result'][-1]['message']['date']
        except:
            return "Stop", 0000
    
    def CheckForDisarmRequest(self):
        Message, Date = self.ReadLatestMessages()
        DisarmList = ["quit","Quit","stop","Stop","disarm","Disarm"]
        for keyword in DisarmList:
            if (Message == keyword and self.Armed == True and self.ForceArmed == False):
                Disarm = 1
                #print("1hier ist fehler")
                #print("force armed:", self.ForceArmed)
                self.Armed = False
                self.SendMessage("Disarmed Surveillance Camera!")
                break
            elif (Message == keyword and self.Armed == True and self.ForceArmed == True):
                #print("date: ",Date)
                #print("\n")
                #print("ForceDate: ",self.DateForceArmed)
                if(Date == self.DateForceArmed):
                    #print("bleibt armed!!!")
                    self.Armed = True # Bleibt true wenn force armed und latest message = alte message which stopped the arm process
                else:
                    #print("2hier ist fehler")
                    self.Armed = False
                    self.ForceArmed = False
                    self.SendMessage("Disarmed Surveillance Camera!")
                    break
            else:
                #self.SendMessage("Surveillance Camera already Disarmed!")
                Disarm = 0
        
        return Disarm
    
    def CheckForArmRequest(self):
        Message, Date = self.ReadLatestMessages()
        ArmList = ["start","Start","Arm","arm","Active","active","activate","Activate"]
        for keyword in ArmList:
            if (Message == keyword and self.Armed == False):
                Arm = 1
                self.Armed = True
                self.SendMessage("ARMED Surveillance Camera!")
                self.ForceArmed = False
                break
            else:
                #self.SendMessage("Surveillance Camera already ARMED!")
                Arm = 0
        
        return Arm

    def Arm_Disarm_ConstrainsCheck(self):
        tz = pytz.timezone('Europe/Berlin')
        berlin_now = datetime.now(tz).time()  
        Message, Date = self.ReadLatestMessages()      
        if berlin_now > self.ArmedByTimeLower and berlin_now < self.ArmedByTimeUpper and self.Armed == False:
            if self.Armed == False:
                self.Armed = True
                self.DateForceArmed = Date
                self.ForceArmed = True                                                                  #used that it is activated past 23:00 even if the last message in chat was "stop"
                self.SendMessage("Automatically ARMED Surveillance Camera because it's past 23:00!")
        else:
            pass
        
    def GetArmedState(self):
        either = self.Arm_Disarm_ConstrainsCheck()
        disarm = self.CheckForDisarmRequest()
        arm = self.CheckForArmRequest()
        
        return self.Armed

    def CheckForLivePicRequest(self):
        #print("checking live")
        Message, Date = self.ReadLatestMessages()
        #print(Message)
        PicList = ["live","Live","foto","Foto"]
        Req = False
        for keyword in PicList:
            if (Message == keyword):
                Req = True
                #print("send live!")
                break
            else:
                #self.SendMessage("Surveillance Camera already ARMED!")
                Req = False
        return Req
# test = TelegramApi()
# counter = 0
# while True:
#     state = test.GetArmedState()
#     if(state == True):
#         if(test.CheckForDisarmRequest()==1):
#             counter = counter +1
#     else:
#         if(test.CheckForArmRequest()==1):
#             counter = counter +1
            
#     if(counter>3):
#         break