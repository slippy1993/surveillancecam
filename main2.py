# Import packages
import os
import argparse
import cv2
import numpy as np
import sys
import glob
import importlib.util
from skimage.metrics import structural_similarity as ssim
from datetime import datetime
from TelegramApi import TelegramApi
from detect import DetectPerson
from HandleCam import HandleCam
#from picamera import PiCamera
import picamera
import picamera.array
import io
from time import sleep
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
import time
import psutil

#prior_image = None
prior_image_name = None




def get_cpu_usage_pct():
    """
    Obtains the system's average CPU load as measured over a period of 500 milliseconds.
    :returns: System CPU load as a percentage.
    :rtype: float
    """
    return psutil.cpu_percent(interval=0.5)

def get_ram_usage():
    """
    Obtains the absolute number of RAM bytes currently in use by the system.
    :returns: System RAM usage in bytes.
    :rtype: int
    """
    return int(psutil.virtual_memory().total - psutil.virtual_memory().available)


def GetLivePicture(stream,camera,Bot):
    global prior_image
    camera.capture(stream, format='bgr')
    # At this point the image is available as stream.array
    image = stream.array
    
    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d-%m-%Y-%H:%M:%S")
    #CWD_PATH = os.getcwd()
    #PathAndName = CWD_PATH+'/'+dt_string+".jpg"
    #CamHandle.CapturePicture(PathAndName)
    
    #cv2.imwrite(PathAndName,OneFrame)
    NameOfPic = dt_string+".jpg"
    cv2.imwrite(NameOfPic,image)
    #stream.truncate(0)
    #stream.seek(0)
    Bot.SendLive(NameOfPic)
    os.remove(NameOfPic)
    result = False
    return result

def compare_images(current,prior):
    #NameOfPic_prior = "prior.jpg"
    #NameOfPic_current = "current.jpg"
    #current.save(NameOfPic_prior)
    #prior.save(NameOfPic_current)

    original = cv2.imread("./"+current)
    contrast = cv2.imread("./"+prior)

    # convert the images to grayscale
    original = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
    contrast = cv2.cvtColor(contrast, cv2.COLOR_BGR2GRAY)
    s = ssim(original, contrast)
    #print("s is: ", s)
    #os.remove(NameOfPic_current)
    #os.remove(NameOfPic_prior)
    return s

def detect_motion(stream,camera,Bot):
    global prior_image_name
    camera.capture(stream, format='bgr')
    # At this point the image is available as stream.array
    image = stream.array
    #image = image[:, :, ::-1]

    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d-%m-%Y-%H:%M:%S")
    #CWD_PATH = os.getcwd()
    #PathAndName = CWD_PATH+'/'+dt_string+".jpg"
    #CamHandle.CapturePicture(PathAndName)
    
    #cv2.imwrite(PathAndName,OneFrame)
    NameOfPic = dt_string+".jpg"
    Images = [NameOfPic]
    argsmodeldir = "."
    argsgraph = "/home/pi/Documents/SmartHome/srcs/object_detection/ssd_mobilenet_v1_1_metadata_1.tflite" #needs to be downlaoded 
    argslabels = "/home/pi/Documents/SmartHome/srcs/object_detection/labelmap.txt" 
    argstreshold = 0.5
    argsimage = "/home/pi/Documents/SmartHome/srcs/object_detection/Code/"
    
    if prior_image_name is None:
        cv2.imwrite(NameOfPic,image)
        prior_image_name = NameOfPic
        #DegreeOfSimiliarity = compare_images(current_image, prior_image)
        #if(DegreeOfSimiliarity > 0.945):
         #   argstreshold = 0.6
        #print("in wrong state")
        for j in Images:
            DetectPerson(Bot,argsimage+j,argsmodeldir,argsgraph,argslabels,argstreshold=0.5,argsimagedir=None)
            #os.remove(NameOfPic)
        return False
    else:
        current_image = image
        cv2.imwrite(NameOfPic,current_image)
        DegreeOfSimiliarity = compare_images(NameOfPic, prior_image_name)
        #print("Degree of sim is: ", DegreeOfSimiliarity)
        if(DegreeOfSimiliarity > 0.93):                                     #additional check to reduce noise
            #print("degree is: ",DegreeOfSimiliarity)
            argstreshold = 0.6
        # Compare current_image to prior_image to detect motion. This is
        # left as an exercise for the reader!
        for j in Images:
            DetectPerson(Bot,argsimage+j,argsmodeldir,argsgraph,argslabels,argstreshold=argstreshold,argsimagedir=None)
            os.remove(prior_image_name)
            prior_image_name = NameOfPic
            
    #stream.truncate(0)
    #stream.seek(0)  
    result = False
    return result



def WriteFileForWatchDog():
    with open('WatchDog.txt', 'w') as writer:
        #writer.truncate(0)
        writer.write("Script runs!")



if __name__ == "__main__":
    Bot = TelegramApi()
    Bot.SendMessage("A Restart/Reboot Occured! Check if State of Surveillance Camera is as desired!")
    #providing the path of the folder
    #r = raw string literal
    folder_path = (r'/home/pi/Documents/SmartHome/srcs/object_detection/Code')
    #using listdir() method to list the files of the folder
    test = os.listdir(folder_path)
    #taking a loop to remove all the images
    #using ".jpg" extension to remove only jpg images
    #using os.remove() method to remove the files
    for images in test:
        if images.endswith(".jpg"):
            os.remove(os.path.join(folder_path, images))
    with picamera.PiCamera() as camera:
        camera.start_preview()
        time.sleep(2)
        try:
            while True:
                with picamera.array.PiRGBArray(camera) as stream:
                    WriteFileForWatchDog()

                    if(Bot.CheckForLivePicRequest()):
                        GetLivePicture(stream,camera,Bot)
                        continue
                    if(Bot.GetArmedState()):
                        #print("armed")
                        #camera.wait_recording(1) kommentar wegmachen wenn gefiltm wird
                        if detect_motion(stream,camera,Bot):
                            pass
                            #p('Motion detected!')
                            # As soon as we detect motion, split the recording to
                            # record the frames "after" motion
                            #camera.split_recording('after.h264')
                            # Write the 10 seconds "before" motion to disk as well
                            #stream.copy_to('before.h264', seconds=10)
                            #stream.clear()
                            # Wait until motion is no longer detected, then split
                            # recording back to the in-memory circular buffer
                            #while detect_motion(camera,Bot):
                                #camera.wait_recording(1)
                            #print('Motion stopped!')
                            #camera.split_recording(stream)
                    else:
                        #print("disarmed")
                        time.sleep(5)
        finally:
            pass 
    
